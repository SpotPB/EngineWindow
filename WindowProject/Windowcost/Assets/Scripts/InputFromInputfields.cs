﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFromInputfields : MonoBehaviour {
    [SerializeField]
    InputField Lengte, Breedte;
    [SerializeField]
    Dropdown Standard, HR, HR2, HR3;//Glass
    [SerializeField]
    Dropdown Wood, Composite, Vinyl;//Frame

    public static float LengthFloat;
    public static float BreedteFloat;
    //Glass
    public static float StandardFloat = 30;
    public static float HRFloat = 45;
    public static float HR2Float = 50;
    public static float HR3Float = 55;
    //Frame
    public static float WoodFloat = 3;
    public static float CompositeFloat = 7;
    public static float VinylFloat = 12;
    
    
    void Update ()
    {
        LengthFloat = float.Parse(Lengte.text);
        BreedteFloat = float.Parse(Breedte.text);
        //Standard.value;
	}
}
